echo "Pushing grafana to ECR"
NAME=grafana
ROOT_ECR_REPO=461756540303.dkr.ecr.us-east-1.amazonaws.com
TAG=$(git log -1 --pretty=$(date +"%Y%m%d%H%M%S")_%H)
$(aws ecr get-login --no-include-email --region us-east-1) || exit 1
docker build . -t $NAME
docker tag $NAME $ROOT_ECR_REPO/$NAME:$TAG || exit 1
docker push $ROOT_ECR_REPO/$NAME:$TAG || exit 1
